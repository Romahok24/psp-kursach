﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Enums
{
    public enum State : byte
    {
        Nothing,
        Wall,
        FirstPlayer,
        SecondPlayer,
        FirstPlayerWall,
        SecondPlayerWall,
        TemporaryWall,
        FirstPlayerTemporaryWall,
        SecondPlayerTemporaryWall,
        HaveNo,
        FirstPlayerInWall,
        SecondPlayerInWall,
        Tresure,
        Prize,
        IncreasePrize,
        DecreasePrize,
        DestructableWall
    }

}
