﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Server.Enums;

namespace Server.Models
{
    public class Player
    {    
        private TcpClient client;
        private Game game;

        public int Id { get; set; }
        public string Name { get; set; }
        public byte Element { get; set; }
        public int X { get; set; }
        public int Y{ get; set; }
        public int Speed { get; set; } = 1;
        public int Value { get; set; }
        public NetworkStream Stream { get; private set; }


        public Player(int id, TcpClient tcpClient, Game game)
        {
            Id = id;
            client = tcpClient;
            this.game = game;
            game.AddConnection(this);
        }

        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                string[] message;
                while (true)
                {
                    try
                    {
                        message = GetMessage().Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                        Enums.Action action;
                        Enum.TryParse(message[0], out action);

                        Direction direction;
                        Enum.TryParse(message[1], out direction);

                        switch (action) 
                        {
                            case Enums.Action.Move: game.MakeMove(this, direction); break;
                            default: game.BuildOrDestruct(this, direction); break;
                        }
                    }
                    catch
                    {
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                game.RemoveConnection(this.Id);
                Close();
            }
        }

        // чтение входящего сообщения и преобразование в строку
        private string GetMessage()
        {
            StringBuilder builder = new StringBuilder();
            byte[] data = new byte[64];            
            int bytes = 0;

            do
            {
                bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);

            return builder.ToString();
        }

        // закрытие подключения
        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }

    }
}
