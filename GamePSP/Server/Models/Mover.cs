﻿using Server.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Models
{
    public class Mover
    {
        private static int moveCount;

        public delegate void InitPrize();
        public event InitPrize onInit;

        public byte[,] Move(byte[,] map, Player player, Direction direction)
        {
            switch (direction)
            {
                case Direction.Forward: MoveForward(map, player); break;
                case Direction.Back: MoveBack(map, player); break;
                case Direction.Right: MoveRight(map, player); break;
                default: MoveLeft(map, player); break;
            }

            moveCount++;

            if (moveCount == 40)
            {
                onInit?.Invoke();
                moveCount = 0;
            }

            return map;
        }

        public byte[,] BuidOrDestructWall(byte[,] map, Player player, Direction direction)
        {
            int x = 0;
            int y = 0;

            if (direction == Direction.Forward)
            {
                DoBuildOrDestruct(map, player.X - 1, player.Y, player.Element);
                x = player.X - 1;
                y = player.Y;
            }

            if (direction == Direction.Back)
            {
                DoBuildOrDestruct(map, player.X + 1, player.Y, player.Element);
                x = player.X + 1;
                y = player.Y;
            }

            if (direction == Direction.Right)
            {
                DoBuildOrDestruct(map, player.X, player.Y + 1, player.Element);
                x = player.X;
                y = player.Y + 1;
            }

            if (direction == Direction.Left)
            {
                DoBuildOrDestruct(map, player.X, player.Y - 1, player.Element);
                x = player.X;
                y = player.Y - 1;
            }

            for(int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (x == i && j == y)
                        continue;

                    if (2 + player.Element == map[i, j])
                        map[i, j] = (byte)State.Nothing;

                    if (5 + player.Element == map[i, j])
                        map[i, j] = (byte)State.TemporaryWall;
                }
            }

            return map;
        }

        private void MoveForward(byte[,] map, Player player)
        {
            for (int i = 0; i < player.Speed; i++)
                DoMove(map, player.X - 1, player.Y, player.X, player.Y, player, -1, 0);
        }

        private void MoveBack(byte[,] map, Player player)
        {
            for (int i = 0; i < player.Speed; i++)
                DoMove(map, player.X + 1, player.Y, player.X, player.Y, player, 1, 0);

        }

        private void MoveRight(byte[,] map, Player player)
        {
            for (int i = 0; i < player.Speed; i++)
                DoMove(map, player.X, player.Y + 1, player.X, player.Y, player, 0, 1);
        }

        private void MoveLeft(byte[,] map, Player player)
        {
            for (int i = 0; i < player.Speed; i++)
                DoMove(map, player.X, player.Y - 1, player.X, player.Y, player, 0, -1);
        }

        private void DoMove(byte[,] map, int newX, int newY, int oldX, int oldY, Player player, int incX, int incY)
        {
            if (map[newX, newY] == (byte)State.Nothing)
            {
                map[newX, newY] = player.Element;
                map[oldX, oldY] = map[oldX, oldY] == (byte)(player.Element + 8) ? (byte)(player.Element + 5) : (byte)State.Nothing;
                player.X += incX;
                player.Y += incY;
            }
            else if (map[newX, newY] == (byte)State.Tresure)
            {
                map[newX, newY] = player.Element;
                map[oldX, oldY] = map[player.X, oldY] == (byte)(player.Element + 8) ? (byte)(player.Element + 5) : (byte)State.Nothing;

                player.X += incX;
                player.Y += incY;
                player.Value += 50;
            }
            else if (map[newX, newY] == (byte)(player.Element + 5))
            {
                map[newX, newY] = (byte)(player.Element + 8);
                map[oldX, oldY] = (byte)State.Nothing;
                player.X += incX;
                player.Y += incY;
            }
            else if (map[newX, newY] == (byte)State.IncreasePrize)
            {
                map[newX, newY] = player.Element;
                map[oldX, oldY] = map[oldX, oldY] == (byte)(player.Element + 8) ? (byte)(player.Element + 5) : (byte)State.Nothing;

                player.X += incX;
                player.Y += incY;
                player.Speed += 1;
            }
            else if (map[newX, newY] == (byte)State.DecreasePrize)
            {
                map[newX, newY] = player.Element;
                map[oldX, oldY] = map[oldX, oldY] == (byte)(player.Element + 8) ? (byte)(player.Element + 5) : (byte)State.Nothing;

                player.X += incX;
                player.Y += incY;
                player.Speed = player.Speed == 1 ? player.Speed : --player.Speed;
            }
        }

        private void DoBuildOrDestruct(byte[,] map, int x, int y, byte element)
        {
            if (map[x, y] == (byte)State.Nothing)
                map[x, y] = (byte)(element + 2);
            else if (map[x, y] == (byte)(element + 2))
                map[x, y] = (byte)State.Nothing;
            else if (map[x, y] == (byte)State.TemporaryWall)
                map[x, y] = (byte)(element + 5);
            else if (map[x, y] == (byte)State.DestructableWall)
                map[x, y] = (byte)State.Nothing;
        }
    }
}
