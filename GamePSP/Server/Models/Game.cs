﻿using Server.Enums;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server.Models
{
    public class Game : INotify
    {
        private static TcpListener tcpListener;

        private List<Player> players = new List<Player>();        
        private Mover mover;
        private Mapper map;

        protected internal void Listen()
        {
            try
            {
                mover = new Mover();
                map = new Mapper();

                map.ReadBmp();
                mover.onInit += map.GeneratePrize;

                tcpListener = new TcpListener(IPAddress.Any, 8888);
                tcpListener.Start();

                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true)
                {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();
                    Player player = new Player(players.Count + 1, tcpClient, this) { X = 8, Y = 1, Element = 2, Name = "Blue Player" };
                    Thread clientThread = new Thread(new ThreadStart(player.Process));
                    clientThread.Start();

                    TcpClient tcpClient1 = tcpListener.AcceptTcpClient();
                    Player player1 = new Player(players.Count + 1, tcpClient1, this) { X = 1, Y = 13, Element = 3, Name = "White Player" };
                    Thread clientThread1 = new Thread(new ThreadStart(player1.Process));
                    clientThread1.Start();

                    if (players.Count == 2)
                        Notify();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        public void MakeMove(Player player, Direction direction)
        {
            mover.Move(map.Map, player, direction);
            Notify();
        }

        internal void BuildOrDestruct(Player player, Direction direction)
        {
            mover.BuidOrDestructWall(map.Map, player, direction);
            Notify();
        }

        public void Notify()
        {
            if (!map.HasGold)
            {
                string winner = $"Winner : { GetWinner(players[0], players[1])}";
                var array = Encoding.Unicode.GetBytes(winner);

                foreach (var player in players)
                    player.Stream.Write(array, 0, array.Length);
            }

            foreach (var player in players)
                player.Stream.Write(map.Array, 0, map.Array.Length);         
        }

        protected internal void AddConnection(Player player)
        {
            players.Add(player);
        }

        protected internal void RemoveConnection(int id)
        {
            Player player = players.FirstOrDefault(c => c.Id == id);
            if (player != null)
                players.Remove(player);
        }

        protected internal void Disconnect()
        {
            tcpListener.Stop();

            for (int i = 0; i < players.Count; i++)
                players[i].Close();

            Environment.Exit(0);
        }

        private string GetWinner(Player player1, Player player2)
        {
            return player1.Value == player2.Value ? "ничья" : (player1.Value > player2.Value ? player1.Name : player2.Name);
        }
    }
}
