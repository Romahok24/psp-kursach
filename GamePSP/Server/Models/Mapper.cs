﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Models
{
    class Mapper
    {
        public byte[,] Map { get; set; } = new byte[10, 15];
        public byte[] Array => Map.Cast<byte>().ToArray();
        public bool HasGold => Array.Contains((byte)12);

        public void SaveBmp()
        {
            Bitmap bmp = new Bitmap(15, 10);

            for (int i = 0; i < Map.GetLength(1); i++)
            {
                for (int j = 0; j < Map.GetLength(0); j++)
                {
                    Color color;
                    switch (Map[j, i])
                    {
                        case 1: color = Color.Green; break;
                        case 2: color = Color.Blue; break;
                        case 3: color = Color.White; break;
                        case 6: color = Color.Red; break;
                        case 12: color = Color.Gold; break;
                        default: color = Color.DarkGray; break;
                    }

                    bmp.SetPixel(i, j, color);
                }
            }
            bmp.Save("3.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
        }

        public void ReadBmp()
        {
            Random rnd = new Random();
            int file = 1;

            if (rnd.Next(20) % 2 == 0)
                file = 2;

            Bitmap bmp = new Bitmap(Image.FromFile($"{2}.bmp"));

            for (int x = 0; x < bmp.Height; x++)
            {
                for (int y = 0; y < bmp.Width; y++)
                {
                    var pixel = bmp.GetPixel(y, x);
                    if (pixel.ToArgb() == Color.Green.ToArgb())
                        Map[x, y] = 1;
                    if (pixel.ToArgb() == Color.Blue.ToArgb())
                        Map[x, y] = 2;
                    if (pixel.ToArgb() == Color.White.ToArgb())
                        Map[x, y] = 3;
                    if (pixel.ToArgb() == Color.Red.ToArgb())
                        Map[x, y] = 6;
                    if (pixel.ToArgb() == Color.Gold.ToArgb())
                        Map[x, y] = 12;
                    if (pixel.ToArgb() == Color.DarkGray.ToArgb())
                        Map[x, y] = 16;
                }
            }
        }

        public void GeneratePrize()
        {
            Random rnd = new Random();
            bool yes = true;
            byte prize = 15;

            if (rnd.Next(10) % 2 == 0)
                prize = 14;

            while (yes)
            {
                int i = rnd.Next(9);
                int j = rnd.Next(14);

                if (Map[i, j] == 0)
                {
                    Map[i, j] = prize;
                    yes = false;
                }
            }
        }
    }
}
