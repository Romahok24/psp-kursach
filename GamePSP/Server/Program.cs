﻿using Server.Models;
using System;
using System.Threading;

namespace Server
{
    class Program
    {
        static Game server;
        static Thread listenThread;
        static void Main(string[] args)
        {
            try
            {
                server = new Game();
                listenThread = new Thread(new ThreadStart(server.Listen));
                listenThread.Start(); //старт потока
            }
            catch (Exception ex)
            {
                server.Disconnect();
                Console.WriteLine(ex.Message);
            }
        }
    }

}
