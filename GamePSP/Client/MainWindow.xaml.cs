﻿using Client.Models;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Input;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ClientModel model;

        public MainWindow()
        {
            InitializeComponent();

            model = new ClientModel(new TcpClient(), this);

            model.InitializeClient();
        }

        private void WindowKeyUp(object sender, KeyEventArgs e) => model.TakeMove(e);
    }
}
