﻿using Client.Models;
using Client.Models.Decorator;
using Client.Models.Factory_Method;
using Server.Enums;
using SharpDX;
using System.Collections.Generic;

namespace Client
{
    class GameControl : D2dControl.D2dControl
    {
        private List<Element> elements = new List<Element>();
        private bool needTorender = true;
        private byte[,] matrix = null;

        public GameControl()
        {
            InitElement();
        }

        public override void Render()
        {
            if (needTorender)
            {
                d2DRenderTarget.Clear(SharpDX.Color.Black);

                foreach (var element in elements)
                    element.Draw(d2DRenderTarget, d2DFactory);

                needTorender = false;
            }
        }

        private void InitElement()
        {
            if (matrix == null)
                return;

            elements.Clear();

            int inc = 30;
            float width = 30;
            float height = 30;

            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    if (matrix[j, i] == (byte)State.Wall)
                        elements.Add(new UnbreacableWallElement(new BaseElement(i * inc, j * inc, width, height, Color.LawnGreen)));

                    if (matrix[j, i] == (byte)State.FirstPlayer || matrix[j, i] == (byte)State.FirstPlayerInWall)
                        elements.Add(new PlayerElement(new BaseElement(i * inc + 15, j * inc + 15, width, height, Color.Blue)));

                    if (matrix[j, i] == (byte)State.SecondPlayer || matrix[j, i] == (byte)State.SecondPlayerInWall)
                        elements.Add(new PlayerElement(new BaseElement(i * inc + 15, j * inc + 15, width, height, Color.White)));

                    if (matrix[j, i] == (byte)State.FirstPlayerWall)
                        elements.Add(new PlayerWallElement(new BaseElement(i * inc, j * inc, width, height, Color.Blue)));

                    if (matrix[j, i] == (byte)State.SecondPlayerWall)
                        elements.Add(new PlayerWallElement(new BaseElement(i * inc, j * inc, width, height, Color.White)));

                    if (matrix[j, i] == (byte)State.TemporaryWall)
                        elements.Add(new TemporaryWallElement(new BaseElement(i * inc, j * inc, width, height, Color.Red)));

                    if (matrix[j, i] == (byte)State.FirstPlayerTemporaryWall)
                        elements.Add(new TemporaryWallElement(new BaseElement(i * inc, j * inc, width, height, Color.Blue)));

                    if (matrix[j, i] == (byte)State.SecondPlayerTemporaryWall)
                        elements.Add(new TemporaryWallElement(new BaseElement(i * inc, j * inc, width, height, Color.White)));

                    if (matrix[j, i] == (byte)State.Tresure)
                        elements.Add(new TreasureElement(new BaseElement(i * inc + 15, j * inc + 15, 2, 2, Color.Gold)));

                    if (matrix[j, i] == (byte)State.IncreasePrize)
                        elements.Add(new IncreasePrizeFactory().Generate(i * inc + 15, j * inc + 15, 4, 4, Color.Purple));

                    if (matrix[j, i] == (byte)State.DecreasePrize)
                        elements.Add(new DecreasePrizeFactory().Generate(i * inc + 15, j * inc + 15, 4, 4, Color.Aqua));

                    if (matrix[j, i] == (byte)State.DestructableWall)
                        elements.Add(new DestructableWall(new BaseElement(i * inc, j * inc, width, height, Color.Gray)));
                }
            }

            needTorender = true;
        }

        internal void SetMatrix(byte[,] map)
        {
            matrix = map;
            InitElement();
        }
    }
}
