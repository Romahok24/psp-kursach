﻿using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Mathematics.Interop;

namespace Client.Models.Factory_Method
{
    class IncreasePrize : Prize
    {
        public IncreasePrize(int x, int y, float width, float height, Color color) : base(x, y, width, height, color)
        {
        }

        public override void Draw(RenderTarget target, SharpDX.Direct2D1.Factory factory)
        {
            var ellipse = new Ellipse(new RawVector2(X, Y), Width, Height);
            var brush = new SolidColorBrush(target, SharpDX.Color.Purple);
            target.DrawEllipse(ellipse, brush);
        }
    }
}
