﻿using SharpDX;

namespace Client.Models.Factory_Method
{
    abstract class Factory
    {
        public abstract Prize Generate(int x, int y, float width, float height, Color color);
    }
}
