﻿using SharpDX;

namespace Client.Models.Factory_Method
{
    class DecreasePrizeFactory : Factory
    {
        public override Prize Generate(int x, int y, float width, float height, Color color)
        {
            return new DecreasePrize(x, y, width, height, color);
        }
    }
}
