﻿using SharpDX;

namespace Client.Models.Factory_Method
{
    public abstract class Prize : Element
    {
        public int AddingSpeed { get; set; }

        public Prize(int x, int y, float width, float height, Color color) : base(x, y, width, height, color)
        {
        }
    }
}
