﻿using SharpDX;

namespace Client.Models.Factory_Method
{
    class IncreasePrizeFactory : Factory
    {
        public override Prize Generate(int x, int y, float width, float height, Color color)
        {
            return new IncreasePrize(x, y, width, height, color);
        }
    }
}
