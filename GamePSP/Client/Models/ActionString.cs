﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Models
{
    public class ActionString
    {
        public static string MoveForward = "Move : Forward";
        public static string MoveBack = "Move : Back";
        public static string MoveLeft = "Move : Left";
        public static string MoveRight = "Move : Right";

        public static string BuildOrdestructForward = "BuildOrdestruct : Forward";
        public static string BuildOrdestructBack = "BuildOrdestruct : Back";
        public static string BuildOrdestructRight = "BuildOrdestruct : Right";
        public static string BuildOrdestructLeft = "BuildOrdestruct : Left";
    }
}
