﻿using SharpDX.Direct2D1;
using SharpDX.Mathematics.Interop;

namespace Client.Models.Decorator
{
    class TreasureElement : ElementDecorator
    {
        public TreasureElement(Element element)
                    : base(element)
        {
        }

        public override void Draw(RenderTarget target, Factory factory)
        {
            var ellipse = new Ellipse(new RawVector2(X, Y), Width, Height);
            var brush = new SolidColorBrush(target, SharpDX.Color.Gold);
            target.DrawEllipse(ellipse, brush);
        }
    }
}
