﻿using SharpDX.Direct2D1;

namespace Client.Models.Decorator
{
    public class UnbreacableWallElement : ElementDecorator
    {
        public UnbreacableWallElement(Element element)
                    : base(element)
        {
        }

        public override void Draw(RenderTarget target, SharpDX.Direct2D1.Factory factory)
        {
            var rectangleGeometry = new RoundedRectangleGeometry(factory, new RoundedRectangle() { RadiusX = 0, RadiusY = 0, Rect = new SharpDX.RectangleF(X, Y, Width, Height) });
            var brush = new SolidColorBrush(target, SharpDX.Color.Green);
            target.FillGeometry(rectangleGeometry, brush);

            var rectangleGeometry1 = new RoundedRectangleGeometry(factory, new RoundedRectangle() { RadiusX = 0, RadiusY = 0, Rect = new SharpDX.RectangleF(X, Y, Width, Height) });
            var brush1 = new SolidColorBrush(target, SharpDX.Color.DarkRed);
            target.DrawGeometry(rectangleGeometry1, brush1);
        }
    }
}