﻿using SharpDX;
using SharpDX.Direct2D1;

namespace Client.Models.Decorator
{
    public class BaseElement : Element
    {
        public BaseElement(int x, int y, float width, float height, Color color) : base(x, y, width, height, color)
        {
        }

        public override void Draw(RenderTarget target, Factory factory)
        {
            var rectangleGeometry = new RoundedRectangleGeometry(factory, new RoundedRectangle() { RadiusX = 0, RadiusY = 0, Rect = new SharpDX.RectangleF(X, Y, Width, Height) });
            var brush = new SolidColorBrush(target, SharpDX.Color.Black);
            target.DrawGeometry(rectangleGeometry, brush);
        }
    }
}
