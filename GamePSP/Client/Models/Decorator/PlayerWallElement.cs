﻿using SharpDX.Direct2D1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Models.Decorator
{
    class PlayerWallElement : ElementDecorator
    {
        public PlayerWallElement(Element element)
                    : base(element)
        {
        }

        public override void Draw(RenderTarget target, Factory factory)
        {
            var rectangleGeometry = new RoundedRectangleGeometry(factory, new RoundedRectangle() { RadiusX = 0, RadiusY = 0, Rect = new SharpDX.RectangleF(X, Y, Width, Height) });
            var brush = new SolidColorBrush(target, Color);
            target.DrawGeometry(rectangleGeometry, brush);
        }
    }
}
