﻿using SharpDX.Direct2D1;

namespace Client.Models.Decorator
{
    class DestructableWall : ElementDecorator
    {
        public DestructableWall(Element element)
                    : base(element)
        {
        }

        public override void Draw(RenderTarget target, Factory factory)
        {
            var rectangleGeometry = new RoundedRectangleGeometry(factory, new RoundedRectangle() { RadiusX = 0, RadiusY = 0, Rect = new SharpDX.RectangleF(X, Y, Width, Height) });
            var brush = new SolidColorBrush(target, SharpDX.Color.DarkGray);
            target.DrawGeometry(rectangleGeometry, brush);
        }
    }
}
