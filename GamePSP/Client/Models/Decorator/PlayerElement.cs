﻿using SharpDX.Direct2D1;
using SharpDX.Mathematics.Interop;

namespace Client.Models.Decorator
{
    class PlayerElement : ElementDecorator
    {
        public PlayerElement(Element element)
                    : base(element)
        {
        }

        public override void Draw(RenderTarget target, Factory factory)
        {
            var ellipse = new Ellipse(new RawVector2(X, Y), Width / 2, Height / 2);
            var brush = new SolidColorBrush(target, Color);
            target.DrawEllipse(ellipse, brush);
        }
    }
}
