﻿using SharpDX.Direct2D1;

namespace Client.Models.Decorator
{
    class TemporaryWallElement : ElementDecorator
    {
        public TemporaryWallElement(Element element)
                    : base(element)
        {
        }

        public override void Draw(RenderTarget target, Factory factory)
        {
            var rectangleGeometry = new RoundedRectangleGeometry(factory, new RoundedRectangle() { RadiusX = 0, RadiusY = 0, Rect = new SharpDX.RectangleF(X, Y, Width, Height) });
            var brush = new SolidColorBrush(target, Color);
            target.DrawGeometry(rectangleGeometry, brush);
        }
    }
}
