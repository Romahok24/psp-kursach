﻿namespace Client.Models.Decorator
{
    public abstract class ElementDecorator : Element
    {
        Element element;

        public ElementDecorator(Element element) : base(element.X, element.Y, element.Width, element.Height, element.Color)
        {
            this.element = element;
        }
    }

}
