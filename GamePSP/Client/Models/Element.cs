﻿using SharpDX;
using SharpDX.Direct2D1;

namespace Client.Models
{
    public abstract class Element
    {
        public int X { get; set; }
        public int Y { get; set; }

        public float Width { get; set; }
        public float Height { get; set; }

        public Color Color { get; set; }

        public Element(int x, int y, float width, float height, Color color)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            Color = color;
        }

        public abstract void Draw(RenderTarget target, Factory factory);
    }
}
