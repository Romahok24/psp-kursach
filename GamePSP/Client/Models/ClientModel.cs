﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Client.Models
{
    class ClientModel
    {
        private const string host = "127.0.0.1";//"192.168.100.3";
        private const int port = 8888;
        private TcpClient client;
        private NetworkStream stream;
        private MainWindow window;

        public ClientModel(TcpClient client, MainWindow window) 
        {
            this.client = client;
            this.window = window;
        }

        public void InitializeClient()
        {
            try
            {
                client.Connect(host, port); //подключение клиента
                stream = client.GetStream(); // получаем поток

                // запускаем новый поток для получения данных
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start(); //старт потока
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void TakeMove(KeyEventArgs e)
        {
            string direction = ActionString.MoveForward;

            if (e.Key == Key.A)
                direction = ActionString.MoveLeft;

            if (e.Key == Key.D)
                direction = ActionString.MoveRight;

            if (e.Key == Key.S)
                direction = ActionString.MoveBack;

            if (e.Key == Key.Up)
                direction = ActionString.BuildOrdestructForward;

            if (e.Key == Key.Down)
                direction = ActionString.BuildOrdestructBack;

            if (e.Key == Key.Left)
                direction = ActionString.BuildOrdestructLeft;

            if (e.Key == Key.Right)
                direction = ActionString.BuildOrdestructRight;

            SendMessage(direction);
        }

        private void ReceiveMessage()
        {
            do
            {
                try
                {
                    byte[] data = new byte[64]; // буфер для получаемых данных
                    List<byte> list = new List<byte>();
                    int bytes = 0;

                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        list.AddRange(data);
                    }
                    while (stream.DataAvailable);

                    if (!IsGameOver(list))
                    {

                        byte[,] map = new byte[10, 15];
                        int k = 0;

                        for (int i = 0; i < map.GetLength(0); i++)
                        {
                            for (int j = 0; j < map.GetLength(1); j++)
                            {
                                map[i, j] = list[k++];
                            }
                        }

                        window.gameControl.SetMatrix(map);
                    }
                    else 
                        break;
                }
                catch (Exception ex)
                {
                    Disconnect();
                }
            } while (true);
        }

        private void Disconnect()
        {
            if (stream != null)
                stream.Close();//отключение потока
            if (client != null)
                client.Close();//отключение клиента
            Environment.Exit(0); //завершение процесса
        }

        private void SendMessage(string direction)
        {
            byte[] data = Encoding.Unicode.GetBytes(direction);
            stream.Write(data, 0, data.Length);
        }

        private bool IsGameOver(List<byte> list)
        {
            try
            {
                string message = Encoding.Unicode.GetString(list.ToArray(), 0, list.Count);

                var str = Regex.Replace(message, @"\0", "")
                         .Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                if (!str[0].Equals("Winner "))
                    throw new Exception();

                window.Dispatcher.Invoke(() =>  MessageBox.Show($"Победил: {str[1]}"));

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
